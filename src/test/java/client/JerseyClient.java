package client;

import javax.ws.rs.client.Client;

import com.pushock.model.Credentials;
import com.pushock.model.TagModel;
import com.pushock.model.TaskModel;
import org.junit.Test;

import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.sql.Date;
import java.util.GregorianCalendar;


public class JerseyClient {
    private String testToken = "2335g7gkj656nf8ok9k9";
    private String dateString = "2014-9-15";

    @Test
    public void testAuth(){
        Client client = ClientBuilder.newClient();

        Credentials credentials = new Credentials();
        credentials.setEmail("test@test.ua");
        credentials.setPassword("password");

        Response res = client.target("http://localhost:8080/memo-ws/api/user/auth").
                request(MediaType.APPLICATION_JSON).post(Entity.entity(credentials, MediaType.APPLICATION_JSON));

        System.out.println("Output from Server .... \n");
        String output = res.readEntity(String.class);
        System.out.println(output);

    }

    @Test
    public void testRegister(){
        Client client = ClientBuilder.newClient();

        Credentials credentials = new Credentials();
        credentials.setEmail("test2@test.ua");
        credentials.setPassword("password");

        Response res = client.target("http://localhost:8080/memo-ws/api/user/register").
                request(MediaType.APPLICATION_JSON).post(Entity.entity(credentials, MediaType.APPLICATION_JSON));

        System.out.println("Output from Server .... \n");
        String output = res.readEntity(String.class);
        System.out.println(output);

    }

    @Test
    public void testGetTags(){
        Client client = ClientBuilder.newClient();

        Response res = client.target("http://localhost:8080/memo-ws/api/tag/get/" + testToken).request().get();
        System.out.println("Output from Server .... \n");
        String output = res.readEntity(String.class);
        System.out.println(output);
    }

    @Test
    public void testSaveTag(){
        Client client = ClientBuilder.newClient();

        TagModel tag = new TagModel();
        tag.setTagName("Study");
        tag.setColor("3333dd");

        Response res = client.target("http://localhost:8080/memo-ws/api/tag/save/" + testToken)
                .request().post(Entity.entity(tag, MediaType.APPLICATION_JSON));
        System.out.println("Output from Server .... \n");
        String output = res.readEntity(String.class);
        System.out.println(output);
    }

    @Test
    public void testGetTasks(){
        Client client = ClientBuilder.newClient();
        Response res = client.target("http://localhost:8080/memo-ws/api/task/getForDate/" + testToken + "/"
                + dateString).request().get();

        System.out.println("Output from Server .... \n");
        String output = res.readEntity(String.class);
        System.out.println(output);
    }

    @Test
    public void testSaveTask(){
        Client client = ClientBuilder.newClient();

        Response res = client.target("http://localhost:8080/memo-ws/api/task/save/" + testToken)
                .request().post(Entity.entity(createTask(), MediaType.APPLICATION_JSON));
        System.out.println("Output from Server .... \n");
        String output = res.readEntity(String.class);
        System.out.println(output);

        res = client.target("http://localhost:8080/memo-ws/api/task/getForDate/" + testToken + "/"
                + "2014-11-11").request().get();

        System.out.println("Output from Server .... \n");
        output = res.readEntity(String.class);
        System.out.println(output);
    }

    private TaskModel createTask(){
        TaskModel task = new TaskModel();
        task.setTitle("Woohoo");
        task.setTaskDate("2014-11-11 12:00");
        return task;
    }
}
