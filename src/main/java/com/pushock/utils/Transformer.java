package com.pushock.utils;


import com.pushock.entity.Tag;
import com.pushock.entity.Task;
import com.pushock.model.TagModel;
import com.pushock.model.TaskModel;

import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class Transformer {
    public static List<TagModel> toTagModelList(List<Tag> tags){
        List<TagModel> models = new ArrayList<>();
        for (Tag tag: tags) {
            TagModel model = new TagModel();
            model.setId(tag.getId());
            model.setTagName(tag.getTagName());
            model.setColor(tag.getColor());
            models.add(model);
        }
        return models;
    }

    public static List<TaskModel> toTaskModelList(List<Task> tasks){
        List<TaskModel> models = new ArrayList<>();
        for (Task task: tasks) {
            TaskModel model = new TaskModel();
            model.setId(task.getId());
            model.setTitle(task.getTitle());
            model.setTaskDate(getStringDate(task.getTaskDate()));
            model.setDescription(task.getDescription());
            model.setSeverity(task.getSeverity());
            if(task.getTag()!=null) {
                model.setTag(toModel(task.getTag()));
            }
            models.add(model);
        }
        return models;
    }

    public static Tag toEntity(TagModel model) {
        Tag tag = new Tag();
        if(model.getId()!=0){
            tag.setId(model.getId());
        }
        tag.setTagName(model.getTagName());
        tag.setColor(model.getColor());
        return tag;
    }

    public static TagModel toModel(Tag entity) {
        TagModel tag = new TagModel();
        tag.setId(entity.getId());
        tag.setTagName(entity.getTagName());
        tag.setColor(entity.getColor());
        return tag;
    }

    public static Task toEntity(TaskModel model) {
        Task task = new Task();
        task.setId(model.getId());
        task.setTitle(model.getTitle());
        task.setDescription(model.getDescription());
        task.setTaskDate(new Date(getDateFromString(model.getTaskDate()).getTime()));
        task.setSeverity(model.getSeverity());

        return task;
    }

    private static String getStringDate(Date date){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        return sdf.format(date);
    }

    private static java.util.Date getDateFromString(String stringDate) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");

        try {
            return sdf.parse(stringDate);
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }
}
