package com.pushock.model;


public class RegisterResponse extends DefaultResponse {
    private String token;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
