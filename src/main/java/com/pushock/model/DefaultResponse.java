package com.pushock.model;


public class DefaultResponse {
    protected String message;
    protected boolean success;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public DefaultResponse(String message, boolean success) {
        this.message = message;
        this.success = success;
    }

    public DefaultResponse(boolean success) {
        this.success = success;
    }
    public DefaultResponse(){}
}
