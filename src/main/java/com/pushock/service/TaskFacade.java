package com.pushock.service;

import com.pushock.entity.Task;
import com.pushock.entity.User;
import com.pushock.model.DefaultResponse;
import com.pushock.model.TaskModel;
import com.pushock.repository.TaskRepository;
import com.pushock.repository.UserRepository;
import com.pushock.utils.Transformer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import static com.pushock.utils.Queries.*;

@Component
@Transactional
public class TaskFacade {
    @Autowired
    TaskRepository taskRepository;
    @Autowired
    UserRepository userRepository;

    public List<TaskModel> getTasksForDate(String token, String date) throws ParseException {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date d = dateFormat.parse(date);
        User user = userRepository.findOne(String.format(SELECT_USER_BY_TOKEN, token));
        if (user==null){
            return null;
        }
        List<Task> tasks = taskRepository.find("Select t from Task t where taskDate Like'" +
                new java.sql.Date(d.getTime()) + "%'");
        return Transformer.toTaskModelList(tasks);
    }

    public DefaultResponse saveTask(String token, TaskModel taskModel) {
        User user = userRepository.findOne(String.format(SELECT_USER_BY_TOKEN, token));

        if(user==null) {
            return new DefaultResponse(false);
        }
        Task task = prepareEntity(taskModel, user);
        taskRepository.save(task);
        return new DefaultResponse(true);

    }

    public DefaultResponse updateTask(String token, TaskModel taskModel) {
        User user = userRepository.findOne(String.format(SELECT_USER_BY_TOKEN, token));
        if(user==null) {
            return new DefaultResponse(false);
        }
        Task task = prepareEntity(taskModel, user);
        taskRepository.update(task);
        return new DefaultResponse(true);
    }

    public DefaultResponse deleteTask(String token, TaskModel taskModel) {
        User user = userRepository.findOne(String.format(SELECT_USER_BY_TOKEN, token));
        if(user==null) {
            return new DefaultResponse(false);
        }
        Task task = prepareEntity(taskModel, user);
        taskRepository.delete(task);
        return new DefaultResponse(true);
    }

    private Task prepareEntity(TaskModel model, User user){
        Task task = Transformer.toEntity(model);
        task.setUser(user);
        if(model.getTag()!=null){
            task.setTag(Transformer.toEntity(model.getTag()));
        }
        return task;
    }
}
