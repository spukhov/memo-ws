package com.pushock.service;

import com.pushock.entity.User;
import com.pushock.model.AuthResponse;
import com.pushock.model.Credentials;
import com.pushock.model.RegisterResponse;
import com.pushock.repository.UserRepository;
import org.apache.commons.lang3.RandomStringUtils;
import static com.pushock.utils.Constants.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Date;

@Transactional
@Component
public class UserFacade {
    @Autowired
    UserRepository userRepository;

    public AuthResponse authorize(Credentials credentials){
        AuthResponse response = new AuthResponse();
        String query = "Select u from User u where u.email = '" + credentials.getEmail()+ "'";

        User userSearched = userRepository.findOne(query);
        if (userSearched == null) {
            response.setSuccess(false);
        } else {
            response.setSuccess(userSearched.getPassword().equals(credentials.getPassword()));
            response.setToken(userSearched.getToken());
            userRepository.update(userSearched);
        }

        return response;
    }

    public RegisterResponse register(Credentials credentials){
        RegisterResponse registerResponse = new RegisterResponse();
        String query = "Select u from User u where email='" + credentials.getEmail() + "'";
        User testUser = userRepository.findOne(query);
        if(testUser!=null){
            registerResponse.setSuccess(false);
            return registerResponse;
        }
        User user = new User();
        user.setEmail(credentials.getEmail());
        user.setPassword(credentials.getPassword());
        user.setDateCreated(new Date(new java.util.Date().getTime()));
        user.setToken(generateToken());
        User returnedUser = userRepository.save(user);

        registerResponse.setSuccess(true);
        registerResponse.setToken(returnedUser.getToken());
        return registerResponse;
    }

    private String generateToken(){
        return RandomStringUtils.randomAlphanumeric(TOKEN_SIZE);
    }

}
