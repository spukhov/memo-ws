package com.pushock.service;

import com.pushock.entity.Tag;
import com.pushock.entity.User;
import com.pushock.model.DefaultResponse;
import com.pushock.model.TagModel;
import com.pushock.repository.TagRepository;
import com.pushock.repository.UserRepository;
import com.pushock.utils.Transformer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static com.pushock.utils.Queries.SELECT_USER_BY_TOKEN;

@Transactional
@Component
public class TagFacade {
    @Autowired
    TagRepository tagRepository;
    @Autowired
    UserRepository userRepository;

    public List<TagModel> getTags(String token){
        User user = userRepository.findOne(String.format(SELECT_USER_BY_TOKEN, token));
        if (user!=null){
            String query = "Select t from Tag t where t.user.id=%d";
            query = String.format(query, user.getId());
            List<Tag> enitytTags = tagRepository.find(query);
            return Transformer.toTagModelList(enitytTags);
        } else return null;

    }

    public DefaultResponse saveTag(String token, TagModel tagModel){
        String query = String.format(SELECT_USER_BY_TOKEN, token);
        User user = userRepository.findOne(query);
        Tag tag = Transformer.toEntity(tagModel);
        tag.setUser(user);
        tagRepository.save(tag);
        return new DefaultResponse(true);
    }
}
