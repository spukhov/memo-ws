package com.pushock.entity;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "Tag")
public class Tag implements Identity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private long id;
    @Column(name = "tagname")
    private String tagName;
    @Column(name = "color")
    private String color;

    @OneToOne
    @JoinColumn(name = "userid")
    private User user;
    @OneToMany(mappedBy = "tag", fetch = FetchType.LAZY)
    private List<Task> tasks;

    public Tag(){};

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTagName() {
        return tagName;
    }

    public void setTagName(String tagName) {
        this.tagName = tagName;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public List<Task> getTasks() {
        return tasks;
    }

    public void setTasks(List<Task> tasks) {
        this.tasks = tasks;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }
}
