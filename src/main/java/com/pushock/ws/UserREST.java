package com.pushock.ws;

import com.pushock.model.Credentials;
import com.pushock.model.AuthResponse;
import com.pushock.model.RegisterResponse;
import com.pushock.service.UserFacade;
import org.glassfish.jersey.process.internal.RequestScoped;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Component
@Path("user")
public class UserREST {
    @Autowired
    private UserFacade userFacade;

    @POST
    @Path("/auth")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces({MediaType.APPLICATION_JSON})
    public AuthResponse authorize(Credentials credentials){
        return userFacade.authorize(credentials);
    }

    @POST
    @Path("/register")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces({MediaType.APPLICATION_JSON})
    public RegisterResponse register(Credentials credentials){
        return userFacade.register(credentials);
    }

    @GET
    @Path("test")
    public Response test(){
        return Response.status(200)
                .entity("ssss").build();
    }


}
