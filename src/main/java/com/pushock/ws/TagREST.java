package com.pushock.ws;

import com.pushock.model.DefaultResponse;
import com.pushock.model.TagModel;
import com.pushock.service.TagFacade;
import org.springframework.beans.factory.annotation.Autowired;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("/tag")
public class TagREST {
    @Autowired
    TagFacade tagFacade;

    @GET
    @Path("/get/{token}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<TagModel> getTags(@PathParam("token")String token){
        return tagFacade.getTags(token);
    }

    @POST
    @Path("/save/{token}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public DefaultResponse getTags(@PathParam("token")String token, TagModel tagModel){
        return tagFacade.saveTag(token, tagModel);
    }
}
