package com.pushock.ws;

import com.pushock.model.DefaultResponse;
import com.pushock.model.TaskModel;
import com.pushock.service.TaskFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.text.ParseException;
import java.util.List;

@Component
@Path("/task")
public class TaskREST {
    @Autowired
    TaskFacade taskFacade;

    @GET
    @Path("/getForDate/{token}/{date}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<TaskModel> getTasksForDate(@PathParam("token") String token,
                                           @PathParam("date") String date) throws ParseException {
        return taskFacade.getTasksForDate(token, date);
    }

    @POST
    @Path("/save/{token}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public DefaultResponse saveTask(@PathParam("token") String token,
                                    TaskModel taskModel) {
        return taskFacade.saveTask(token, taskModel);
    }

    @POST
    @Path("/update/{token}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public DefaultResponse updateTask(@PathParam("token") String token,
                                    TaskModel taskModel) {
        return taskFacade.updateTask(token, taskModel);
    }

    @POST
    @Path("/remove/{token}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public DefaultResponse removeTask(@PathParam("token") String token,
                                      TaskModel taskModel) {
        return taskFacade.deleteTask(token, taskModel);
    }

}
