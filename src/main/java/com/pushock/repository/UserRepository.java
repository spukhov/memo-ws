package com.pushock.repository;

import com.pushock.entity.User;
import org.springframework.stereotype.Repository;

@Repository
public class UserRepository extends GenericRepositoryImpl<User> {
}
