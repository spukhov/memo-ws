package com.pushock.repository;

import com.pushock.entity.Tag;
import org.springframework.stereotype.Repository;

@Repository
public class TagRepository extends GenericRepositoryImpl<Tag> {
}
