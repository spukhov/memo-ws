package com.pushock.repository;

import org.hibernate.Criteria;

import java.util.List;

/**
 * Created by pooh on 14.09.14.
 */
public interface GenericRepository<T> {
    long count();
    T save(T t);
    void delete(T t);
    void delete(List<T> list);
    void delete(long id);
    void update(T t);
    boolean exists(long id);
    List<T> findAll();
    List<T> find(List<Long> ids);
    List<T> find(String query);
    T findOne(long id);
    T findOne(String querry);
}
