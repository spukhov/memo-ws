package com.pushock.repository;

import com.pushock.entity.Identity;
import com.pushock.entity.User;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class GenericRepositoryImpl<T extends Identity> implements GenericRepository<T> {
    @PersistenceContext
    protected EntityManager em;
    private Class< T > type;
    private String name;

    public GenericRepositoryImpl() {
        Type t = getClass().getGenericSuperclass();
        ParameterizedType pt = (ParameterizedType) t;
        type = (Class) pt.getActualTypeArguments()[0];
        name = type.getSimpleName();
    }

    @Override
    public long count() {
        return findAll().size();
    }

    @Override
    public T save(T t) {
        em.persist(t);
        System.out.println("ID:              " + t.getId());
        return findOne(t.getId());
    }

    @Override
    public void delete(T t) {
        em.remove(t);
    }

    @Override
    public void delete(List<T> list) {
        for (T t : list) {
            em.remove(t);
        }
    }

    @Override
    public void delete(long id) {
        em.remove(em.getReference(type, id));

    }

    @Override
    public void update(T t) {
        em.merge(t);
    }

    @Override
    public boolean exists(long id) {
        return em.contains(em.getReference(type, id));
    }

    @Override
    public List<T> findAll() {
        Query query = em.createQuery("from " + name);
        return query.getResultList();
    }

    @Override
    public List<T> find(List<Long> ids) {
        List<T> list = new ArrayList();
        for (Long id : ids){
            list.add(findOne(id));
        }
        return list;
    }

    @Override
    public List<T> find(String query) {
        Query q = em.createQuery(query);
        return q.getResultList();
    }

    @Override
    public T findOne(long id) {
        return em.getReference(type, id);

    }

    @Override
    public T findOne(String query) {
        Query q = em.createQuery(query);
        List<T> list = (List<T>) q.getResultList();
        if (list == null || list.isEmpty()){
            return null;
        }
        if(list.size()>1){
            throw new RuntimeException("found more than 1 record!");
        }
        return list.get(0);
    }

}
