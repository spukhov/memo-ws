package com.pushock.repository;

import com.pushock.entity.Task;
import org.springframework.stereotype.Repository;

@Repository
public class TaskRepository extends GenericRepositoryImpl<Task> {
}
