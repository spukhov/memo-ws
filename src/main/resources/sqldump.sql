DROP TABLE Task;
DROP TABLE Tag;
DROP TABLE User;

CREATE TABLE User (
  id INT NOT NULL AUTO_INCREMENT,
  email VARCHAR(50) NOT NULL UNIQUE,
  password VARCHAR(50) NOT NULL,
  datecreated DATE NOT NULL,
  token VARCHAR(20),
  PRIMARY KEY  (id)
);

CREATE TABLE Tag (
  id INT NOT NULL AUTO_INCREMENT,
  tagname VARCHAR(50) NOT NULL,
  color VARCHAR(6),
  userid INT ,
  FOREIGN KEY (userid) REFERENCES User(id),
  PRIMARY KEY (id)
);


CREATE TABLE Task (
  id INT NOT NULL AUTO_INCREMENT,
  title VARCHAR(255) NOT NULL,
  taskdate DATE NOT NULL,
  severity INT default 0,
  description VARCHAR(255),
  userid INT NOT NULL ,
  tagid INT DEFAULT 1,
  FOREIGN KEY (userid) REFERENCES User(id),
  FOREIGN KEY (tagid) REFERENCES Tag(id),
  PRIMARY KEY (id)
);



INSERT INTO User (email, password, datecreated, token) VALUES ('test@test.ua', 'password', '2014-09-13', '2335g7gkj656nf8ok9k9');
INSERT INTO Tag (tagname, userid) VALUES ('Default', 1);
INSERT INTO Tag (tagname, userid) VALUES ('Sport', 1);
INSERT INTO Task (title, taskdate, userid) VALUES ('Test Topic', '2014-09-15', 1);

